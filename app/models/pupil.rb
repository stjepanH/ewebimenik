class Pupil < ActiveRecord::Base
  belongs_to :classroom
  belongs_to :user
  attr_accessible :first_name, :last_name, :classroom_id, :user_id
  has_many :pupil_task_points
  has_many :tasks, through: :pupil_task_points

  validates_presence_of :first_name, :last_name

  def full_name
    first_name + ' ' + last_name
  end
end
