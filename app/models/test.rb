class Test < ActiveRecord::Base
  belongs_to :user
  attr_accessible :name, :user_id, :grade_points, :repeating

  has_many :tasks
  has_many :pupil_test_points
  has_many :pupils, through: :pupil_test_points
  has_many :pupil_task_points, through: :tasks

  def grade_intervals
    grade_points.split(',').map(&:to_i)
  end

  def grade (points)
    intervals = grade_intervals
    if points.present? && intervals.present?
      intervals.each_with_index do |grade, index|
        if points <= grade
          return index+1
        end
      end
      5
    end
  end
end
