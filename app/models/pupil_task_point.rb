class PupilTaskPoint < ActiveRecord::Base
  belongs_to :pupil
  belongs_to :task
  attr_accessible :points, :task_id, :pupil_id

  has_one :pupil_test_point, through: :task
end
