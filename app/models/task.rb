class Task < ActiveRecord::Base
  belongs_to :test
  
  attr_accessible :no, :test_id, :max_points

  has_many :pupil_task_points
  has_many :pupils, through: :pupil_task_points
end
