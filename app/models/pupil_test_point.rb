class PupilTestPoint < ActiveRecord::Base
  belongs_to :pupil
  belongs_to :test
  attr_accessible :points, :pupil_id, :test_id
end
