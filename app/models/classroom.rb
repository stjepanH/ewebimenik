class Classroom < ActiveRecord::Base
  attr_accessible :name, :user_id

  belongs_to :user
  has_many :pupils

  validates_presence_of :name, :user_id
end
