# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  points = []
  $('[contenteditable]')
    .focus ->
      points[@id] = $(this).html()
    .keydown (e)->
      if e.keyCode == 13
        e.preventDefault()
        $(this).parent().next().find('td:first').focus()
    .blur ->
      update($(this).html(), @id, this)


  update = (newpoints, id, that) ->
    if points[id] != newpoints
      $.ajax({
        type: 'PUT'
        url: 'pupil_task_points/'+id+'.json'
        data: { 
          pupil_task_point: {
            points: +newpoints
          }
        }
        success: (data) ->
          $(that).parent().removeClass('error')
        error: (data) ->
          $(that).parent().addClass('error')
      })

  cid = 0
  tid = 0

  $('#classrooms_select').change ->
    cid = +$(this).select().val()
    filter_table()

  $('#tests_select').change ->
    tid = +$(this).select().val()
    filter_table()

  filter_table = ->
    if cid != 0 && tid != 0
      $('tr[cid='+cid+'][tid='+tid+']').show()
      $('tr').not('[cid='+cid+'][tid='+tid+']').hide()
    else if cid != 0
      $('tr[cid='+cid+']').show()
      $('tr').not('[cid='+cid+']').hide()
    else if tid != 0
      $('tr[tid='+tid+']').show()
      $('tr').not('[tid='+tid+']').hide()
    else
      $('tr').show()

    $('thead tr').show()
