class ClassroomsController < ApplicationController
  def index
    @classrooms = current_user.classrooms
  end

  def new
    @classroom = current_user.classrooms.new
  end

  def create
    @classroom = current_user.classrooms.new(params[:classroom])
    respond_to do |format|
      if @classroom.save
        format.html { redirect_to new_classroom_path, :notice => "Classroom successfully created."}
        format.js
      else
        format.html { render :action => 'new' }
        format.js
      end
    end
  end

  def edit
    @classroom = current_user.classrooms.find(params[:id])
  end

  def update
    @classroom = current_user.classrooms.find(params[:id])
    if @classroom.update_attributes(params[:classroom])
      redirect_to classrooms_url, :notice  => "Classroom successfully updated."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @classroom = current_user.classrooms.find(params[:id])
    @classroom.destroy
    respond_to do |format|
      format.html { redirect_to classrooms_url, :notice => "Classroom successfully destroyom."}
      format.js
    end
  end
end
