class TestsController < ApplicationController

  def index
    @tests = current_user.tests
  end

  def new
    @test = current_user.tests.new
  end

  def edit
    @test = current_user.tests.find(params[:id])
  end

  def create
    @test = current_user.tests.new(params[:test])

    if @test.save
      if params[:commit].ends_with? 'Add New'
        redirect_to new_test_path, :notice => "Test successfully created."
      else
        redirect_to tests_url, notice: 'Test was successfully created.'
      end
    else
      render action: "new"
    end
  end

  def update
    @test = current_user.tests.find(params[:id])

    if @test.update_attributes(params[:test])
      redirect_to tests_url, notice: 'Test was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @test = current_user.tests.find(params[:id])
    @test.destroy

    redirect_to tests_url
  end

  def apply_test
    @test = current_user.tests.find(params[:test_id])
    @classrooms = current_user.classrooms
  end

  def apply_test_to_pupils
    test = current_user.tests.find(params[:test_id])
    pupils = Pupil.where(id: params[:pupil_ids])
    pupils.each do |pupil|
      test.pupil_test_points.create(pupil_id: pupil.id)
      test.tasks.each do |task|
        task.pupil_task_points.create(pupil_id: pupil.id)
      end
    end
    redirect_to tests_url
  end

  def grades
    @test = current_user.tests.find(params[:test_id])
    @pupil_test_points = @test.pupil_test_points
  end

  def pupil_task_points
    @test = current_user.tests.find(params[:test_id])
    @pupil_task_points = @test.pupil_task_points.order('pupil_id').order('task_id')
    @classrooms = current_user.classrooms
  end

   def update_pupil_task_point
    @pupil_task_point = PupilTaskPoint.find(params[:pupil_task_point_id])
    points = @pupil_task_point.points || 0
    @test = current_user.tests.find(params[:test_id])

    respond_to do |format|
      if params[:pupil_task_point][:points].to_i <= @pupil_task_point.task.max_points && @pupil_task_point.update_attributes(params[:pupil_task_point])
        pupil_test_points = @test.pupil_test_points.find_by_pupil_id(@pupil_task_point.pupil)
        points = (-points) + params[:pupil_task_point][:points].to_i + (pupil_test_points.points || 0)
        pupil_test_points.points = points
        pupil_test_points.save

        format.html { redirect_to pupil_task_points_path, notice: 'Pupil task point was successfully updated.' }
        format.json { render json: @pupil_task_point.to_json }
      else
        format.html { render action: "edit" }
        format.json { render json: {error: 'points > than max points'}, status: 400}
      end  
    end
  end
end
