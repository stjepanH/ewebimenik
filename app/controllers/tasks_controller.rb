class TasksController < ApplicationController

  before_filter :load_test

  def index
    @tasks = @test.tasks
  end

  def new
    @task = @test.tasks.new
    unless params[:tasks_count].nil?
      @next_number = params[:tasks_count].to_i + 1
    else
      @next_number = params[:next_number].to_i + 1
    end
  end

  def edit
    @task = @test.tasks.find(params[:id])
  end

  def create
    params[:task][:user_id] = current_user.id
    @task = @test.tasks.new(params[:task])

    if @task.save
      if params[:commit].ends_with? 'Add New'
        redirect_to new_task_path(test_id: params[:task][:test_id], next_number: params[:task][:no]), :notice => "Task successfully created."
      else
        redirect_to tasks_url, notice: 'Task was successfully created.'
      end
    else
      render action: "new"
    end
  end

  def update
    @task = @test.tasks.find(params[:id])

    if @task.update_attributes(params[:task])
      redirect_to tasks_url, notice: 'Task was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @task = @test.tasks.find(params[:id])
    @task.destroy

    redirect_to tasks_url
  end

  def load_test
    @test = current_user.tests.find(params[:test_id])
  end
end
