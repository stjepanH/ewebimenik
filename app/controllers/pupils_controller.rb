class PupilsController < ApplicationController

  before_filter :load_classroom

  def index
    @pupils = @classroom.pupils
  end

  def new
    @pupil = @classroom.pupils.new
  end

  def edit
    @pupil = @classroom.pupils.find(params[:id])
  end

  def create
    @pupil = @classroom.pupils.new(params[:pupil])

    respond_to do |format|
      if @pupil.save
        format.html { redirect_to new_classroom_pupil_url(@classroom), :notice => "Pupil successfully created." }
        format.js
      else
        format.html { render action: "new" }
        format.js { }
      end
    end
  end

  def update
    @pupil = @classroom.pupils.find(params[:id])

    if @pupil.update_attributes(params[:pupil])
      redirect_to classroom_pupils_url, notice: 'Pupil was successfully updated.' 
    else
      render action: "edit" 
    end
  end

  def destroy
    @pupil = @classroom.pupils.find(params[:id])
    @pupil.destroy
    respond_to do |format|
      format.html { redirect_to classroom_pupils_url }
      format.js
    end
  end

  def load_classroom
    @classroom = current_user.classrooms.find(params[:classroom_id])
  end
end
