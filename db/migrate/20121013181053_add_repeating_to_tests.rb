class AddRepeatingToTests < ActiveRecord::Migration
  def change
    add_column :tests, :repeating, :boolean
  end
end
