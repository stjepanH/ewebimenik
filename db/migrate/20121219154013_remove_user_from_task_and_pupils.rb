class RemoveUserFromTaskAndPupils < ActiveRecord::Migration
  def change
    remove_column :tasks, :user_id
    remove_column :pupils, :user_id
    remove_column :pupil_task_points, :user_id
  end
end
