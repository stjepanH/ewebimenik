class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :no
      t.belongs_to :test
      t.belongs_to :user
      t.integer :max_points

      t.timestamps
    end
    add_index :tasks, :test_id
  end
end
