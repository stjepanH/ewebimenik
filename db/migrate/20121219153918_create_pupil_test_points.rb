class CreatePupilTestPoints < ActiveRecord::Migration
  def change
    create_table :pupil_test_points do |t|
      t.belongs_to :pupil
      t.belongs_to :test
      t.integer :points

      t.timestamps
    end
    add_index :pupil_test_points, :pupil_id
    add_index :pupil_test_points, :test_id
  end
end
