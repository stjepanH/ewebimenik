class CreatePupils < ActiveRecord::Migration
  def change
    create_table :pupils do |t|
      t.string :first_name
      t.string :last_name
      t.belongs_to :classroom
      t.belongs_to :user

      t.timestamps
    end
    add_index :pupils, :classroom_id
  end
end
