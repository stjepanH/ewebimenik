class CreatePupilTaskPoints < ActiveRecord::Migration
  def change
    create_table :pupil_task_points do |t|
      t.belongs_to :user
      t.belongs_to :pupil
      t.belongs_to :task
      t.integer :points

      t.timestamps
    end
    add_index :pupil_task_points, :pupil_id
    add_index :pupil_task_points, :task_id
  end
end
