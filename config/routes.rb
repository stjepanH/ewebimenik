Ewebimenik::Application.routes.draw do

  resources :tests, except: [:show] do
    get 'apply' => 'tests#apply_test'
    post 'apply' => 'tests#apply_test_to_pupils', as: 'apply_to_pupils'
    get 'grades'
    resources :tasks, except: [:show]
    get 'pupil_task_points'
    put 'pupil_task_points/:pupil_task_point_id' => 'tests#update_pupil_task_point', as: 'update_pupil_task_point'
  end

  resources :classrooms, except: [:show] do
    resources :pupils, except: [:show]
  end

  devise_for :users

  root to: 'users#index'
end
